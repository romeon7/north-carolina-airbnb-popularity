from bs4 import BeautifulSoup
import requests, re
import time
from datetime import date
from dateutil.relativedelta import relativedelta
import pandas as pd

# This script will scrape the airbnb website for airbnb availability in  
# the top N pages in North Carolina, 1 month from current date for a 1 week stay

# Nicholas Romeo

AIRBNB_URL = "https://www.airbnb.com"
TOP_LISTINGS = 500
LISTING_CLASS = "c4mnd7m dir dir-ltr"
RULES_SEARCH_PAGE_1 = {
  'title': {'tag': 'div', 'class': 't1jojoys dir dir-ltr'},
  'description': {'tag': 'div', 'class': 'n1v28t5c s1cjsi4j dir dir-ltr'},
  'price_night': {'tag': 'div', 'class': '_1jo4hgw'},
  'rating': {'tag': 'span', 'class': 'ru0q88m dir dir-ltr'},
  'url': {'tag': 'a', 'class': 'ln2bl2p dir dir-ltr'}
}
RULES_SEARCH_PAGE_2 = {
  'location': {'tag': 'span', 'class': '_9xiloll'},
  'house_details': {'tag': 'li', 'class': 'l7n4lsf dir dir-ltr'}
}

def extract_page(page_url):
  
  # Extracts HTML from a webpage
  answer = requests.get(page_url)
  content = answer.content
  
  page_soup = BeautifulSoup(content, features='html.parser')

  # Extracts listings from an Airbnb search page; return
  return page_soup.findAll("div", {"class": LISTING_CLASS})

def extract_page_information(information,listing):
  # Extracts data from a specified HTML element

  for rule in RULES_SEARCH_PAGE_1:

    params = RULES_SEARCH_PAGE_1[rule]
    
    # 1. Find the right tag
    if 'class' in params:
      elements_found = listing.find_all(params['tag'], params['class'])
    else:
      elements_found = listing.find_all(params['tag'])
    
    # 2. Extract text from these tags
    element_texts = [el.get_text() for el in elements_found]

    # Clean up price_night
    # Store discount price over full price per night
    if (rule == 'price_night'):
      prices = str(element_texts[0]).split('\xa0')
      price = prices[0]
      if (len(prices) == 3): price = prices[1]
    
    # Extract url from elements_found
    if (rule == 'url'):
      soup = BeautifulSoup(str(elements_found[0]),"html.parser")
      for a in soup.find_all('a', href=True):
        url = AIRBNB_URL+str(a['href']).split('?')[0]
    
    if   (rule == 'title'):       information[rule] = element_texts[0]
    elif (rule == 'description'): information[rule] = str(element_texts[0]).strip('\n')
    elif (rule == 'beds'):        information[rule] = element_texts[0]
    elif (rule == 'price_night'): information[rule] = price
    elif (rule == 'url'):         information[rule] = url
    elif (rule == 'rating'):
      rating = str(element_texts[0]).split(' ')
      information[rule] = rating[0]
      if (len(rating) != 1):
        information['num_reviews'] = rating[1].replace("(","").replace(")","")      
  
  return information

# Information from s/North-Carolina--USA
#######################################
# Information from /rooms/...

def extract_listing(information,listing_url):

  answer = requests.get(listing_url)
  
  p_lat  = re.compile(r'"lat":([-0-9.]+),')
  p_long = re.compile(r'"lng":([-0-9.]+),')
  information['lat']  = p_lat.findall(answer.text)[0]
  information['long'] = p_long.findall(answer.text)[0]
  
  content = answer.content

  return information,BeautifulSoup(content, features='html.parser')

def extract_listing_information(information,listing):

  for rule in RULES_SEARCH_PAGE_2:

    params = RULES_SEARCH_PAGE_2[rule]
    
    # 1. Find the right tag
    if 'class' in params:
      elements_found = listing.find_all(params['tag'], params['class'])
    else:
      elements_found = listing.find_all(params['tag'])

    # 2. Extract text from these tags
    element_texts = [el.get_text() for el in elements_found]

    if (rule == 'location'):  information['location'] = str(element_texts[0]).split(', ')[0]
    elif (rule == 'house_details'):
      information['guests']   = str(element_texts[0]).strip(' · ').split(' ')[0]
      information['bedrooms'] = str(element_texts[1]).strip(' · ').split(' ')[0]
      information['beds']     = str(element_texts[2]).strip(' · ').split(' ')[0]
      information['baths']    = str(element_texts[3]).strip(' · ').split(' ')[0]

  return information
  
def main():

  today = date.today()
  checkin = today+relativedelta(months=1)
  checkout = checkin+relativedelta(days=7)
  
  if (TOP_LISTINGS >= 20): pages = int(TOP_LISTINGS/20)

  column_names = [
    'id',
    'title',
    'description',
    'location',
    'guests',
    'bedrooms',
    'beds',
    'baths',
    'price_night',
    'rating',
    'num_reviews',
    'url',
    'lat',
    'long'
  ]
  airbnb_df = pd.DataFrame(columns = column_names)
  airbnb_id = 0

  for page in range(pages):
    item_offset = ""
    if (page != 0):
      item_offset = "&items_offset="+str(page*20)+"&section_offset=2"
    
    # URL for Airbnbs in North Caroline, 1 week in advance, for 1 week stay
    north_carolina_airbnb_url = AIRBNB_URL+"/s/North-Carolina--USA/homes?tab_id=home_tab&refinement_paths%5B%5D=%2Fhomes&flexible_trip_lengths%5B%5D=one_week&place_id=ChIJgRo4_MQfVIgRGa4i6fUwP60&date_picker_type=calendar&checkin="+str(checkin)+"&checkout="+str(checkout)+"&source=structured_search_input_header&search_type=autocomplete_click&federated_search_session_id=c97bddf8-53be-43e1-ac79-c1cc5ba49d8a&pagination_search=true"+item_offset

    # Get the page listing of URL (20 per page)
    airbnb_listings_20 = extract_page(north_carolina_airbnb_url)

    # For each listing, extract the following information:
    for listing in airbnb_listings_20:
      print("BNB-"+str(airbnb_id))
      
      information = {
        column_names[0]:"BNB-"+str(airbnb_id),
        column_names[1]:None,
        column_names[2]:None,
        column_names[3]:None,
        column_names[4]:None,
        column_names[5]:None,
        column_names[6]:None,
        column_names[7]:None,
        column_names[8]:None,
        column_names[9]:None,
        column_names[10]:None,
        column_names[11]:None,
        column_names[12]:None,
        column_names[13]:None
      }
      
      # Store information from name -> url
      information = extract_page_information(information,listing)
            
      # Use listing_url to get latitude and longitude; get soup
      information,airbnb_listing = extract_listing(information,information['url'])

      # Extract listing information from room
      information = extract_listing_information(information,airbnb_listing)
      
      # Append information to dataframe
      airbnb_df = airbnb_df.append(information, ignore_index=True)

      airbnb_id = airbnb_id + 1
  airbnb_df.to_csv('data\\airbnb_data.csv', index = False, header=True, sep=',')
  return

if __name__ == "__main__":
  start_time = time.time()
  print("--- Scraping Airbnb Website ---")
  main()
  print("--- %s Seconds ---" % (round(time.time() - start_time,2)))
