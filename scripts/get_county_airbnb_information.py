import pandas as pd
import time

# Group airbyb by county, merge that with county_information.csv

# Nicholas Romeo

COUNTY_FILE = "data\\county_information.csv"
AIRBNB_FILE = "data\\airbnb_data.csv"

def main():

  county_df = pd.read_csv(COUNTY_FILE)
  county_df = county_df.filter(['County','id','GlobalID'])
  
  airbnb_df = pd.read_csv(AIRBNB_FILE)
  print(airbnb_df)
  print("\n")
  airbnb_df = airbnb_df.filter(['location','id'])
  print(airbnb_df)
  print("\n")
  airbnb_df = airbnb_df.rename(columns={"location": "County", "id": "Count"})
  print(airbnb_df)
  print("\n")
  # airbnb_df = airbnb_df.groupby(['location']).count()
  # print(airbnb_df)
  # print("\n")

  airbnb_df = airbnb_df['County'].value_counts().to_frame().reset_index()
  airbnb_df = airbnb_df.rename(columns={"County": "Count", "index": "County"})
  print(airbnb_df['Count'].to_string())
  print(airbnb_df)
  result = county_df.set_index('County').join(airbnb_df.set_index('County'))
  print(result)
  
  # airbnb_locations = airbnb_df.location.values.tolist()
  # print(airbnb_locations)
    
  return

if __name__ == "__main__":
  start_time = time.time()
  print("--- Scraping Airbnb Website ---")
  main()
  print("--- %s Seconds ---" % (round(time.time() - start_time,2)))