import pandas as pd
import time

# This script cleans North_Carolina_State_and_County_Boundary_Polygons.csv

# Nicholas Romeo

COUNTY_DATA_FILE = "data\\North_Carolina_State_and_County_Boundary_Polygons.csv"

def main():
  
  county_df = pd.read_csv(COUNTY_DATA_FILE,sep=';')
  print(county_df)

  county_df = county_df.drop(['Rec_Survey','NCGS_url'], axis=1)
  county_df = county_df.rename(columns={"OBJECTID" : "Id", 
                                        "Longitude" : "Long", 
                                        "Latitude" : "Lat",
                                        "ck_date" : "Date",
                                        "Shape__Area" : "Area",
                                        "Shape__Length" : "Length"}, errors="raise")
  for index, row in county_df.iterrows():
    county_df.loc[index,['Date']] = [str(row['Date']).split(' ')[0]]
    
  
  county_df.to_csv('data\\county_information.csv', index = False, header=True, sep=',')


  return

if __name__ == "__main__":
  start_time = time.time()
  print("--- Scraping Airbnb Website ---")
  main()
  print("--- %s Seconds ---" % (round(time.time() - start_time,2)))