import pandas as pd
import time

# This script will combine the coordinates from airbnb_data.csv and NC_Federal_Lands_and_Indian_Reserves.xlxs

# Nicholas Romeo

AIRBNB_FILE = "data\\airbnb_data.csv"
NC_DATA_FILE = "data\\NC_Federal_Lands_and_Indian_Reserves.xlsx"

def get_airbnb_data():
  airbnb_df = pd.read_csv(AIRBNB_FILE)
  
  airbnb_ids = airbnb_df['id'].tolist()
  category = []
  for id in airbnb_ids:
    category.append('Airbnb')

  airbnb_data = {
    'id'        : airbnb_ids,
    'category'  : category,
    'lat'       : airbnb_df['lat'].tolist(),
    'long'      : airbnb_df['long'].tolist()
  }

  return airbnb_data

def get_map_data():

  nc_df = pd.read_excel(NC_DATA_FILE)

  nc_map_data = {
    'id'        : nc_df['ID'].tolist(),
    'category'  : nc_df['Category'].to_list(),
    'lat'       : nc_df['Lat'].tolist(),
    'long'      : nc_df['Long'].tolist()
  }

  return nc_map_data

def get_list():

  airbnb_df = pd.read_csv(AIRBNB_FILE).filter(['location'])

  return

def main():

  airbnb_data = pd.DataFrame.from_dict(get_airbnb_data())
  nc_map_data = pd.DataFrame.from_dict(get_map_data())

  coordinate_data = pd.concat([nc_map_data, airbnb_data], axis=0)
  coordinate_data.to_csv('data\\coordinate_data.csv', index = False, header=True, sep=',')

if __name__ == "__main__":
  start_time = time.time()
  print("--- Scraping Airbnb Website ---")
  #main()
  get_list()
  print("--- %s Seconds ---" % (round(time.time() - start_time,2)))